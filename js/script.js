const menuToggle = document.querySelector('.menu-toggle');
const body = document.body;

menuToggle.addEventListener('click', () => {
    body.classList.toggle('active'); // 
});




function setupMenuToggle(hoverSelector, subMenuSelector) {
    const hoverElement = document.querySelector(hoverSelector);
    const subMenuElement = document.querySelector(subMenuSelector);
  
    hoverElement.addEventListener('click', (event) => {
      event.preventDefault();
      subMenuElement.classList.toggle('active');
    });
  }
  
  setupMenuToggle('.hover-first', '.sub-menu--first');
  setupMenuToggle('.hover-second', '.sub-menu--second');



    const categoryLinks = document.querySelectorAll('.services-btn__wrap a');
    const serviceItems = document.querySelectorAll('.services-item');
    categoryLinks.forEach((link) => {
        link.addEventListener('click', (event) => {
            event.preventDefault();

            const selectedCategory = link.getAttribute('id').replace('category-', '');

            serviceItems.forEach((item) => {
                item.style.display = 'block';
            });

            if (selectedCategory !== 'all') {
                serviceItems.forEach((item) => {
                    const itemCategory = item.getAttribute('data-id');
                    if (itemCategory !== selectedCategory) {
                        item.style.display = 'none';
                    }
                });
            }
        });
    });
